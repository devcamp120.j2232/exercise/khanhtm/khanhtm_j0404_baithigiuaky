

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `devcamp_exam`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lessons`
--

CREATE TABLE `lessons` (
  `id` bigint(20) NOT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `lesson_code` varchar(10) DEFAULT NULL,
  `lesson_name` varchar(255) DEFAULT NULL,
  `page_` bigint(20) DEFAULT NULL,
  `book_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `lessons`
--

INSERT INTO `lessons` (`id`, `introduce`, `lesson_code`, `lesson_name`, `page_`, `book_id`) VALUES
(1, 'làm quen với ứng dụng lập trình', 'JAVA001', 'Làm Quen Java', 3, 1),
(2, 'Cài đặt và làm quen với công cụ', 'JAVA002', 'Cài Đặt Và Sử Dụng', 4, 1),
(3, 'làm quen với python', 'PYTHON01', 'Tổng quan về Python', 55, 2),
(4, 'làm quen và tập Khai báo biến trong python', 'PYTHON02', 'Khai báo biến trong python', 8, 2),
(5, 'Cài đặt và làm quen với công cụ', 'CSS01', 'Ngôn ngữ Css', 11, 3),
(6, 'Cài đặt và làm quen với công cụ', 'JS012', 'javaScrip', 11, 3),
(9, 'Cài đặt và làm quen với công cụ', 'CSS', 'Ngôn ngữ Css', 11, 3),
(10, 'Cài đặt và làm quen với công cụ', 'CSC', 'Ngôn ngữ Css', NULL, 3);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_7jvsiodwrlie5ajrom650ofuw` (`lesson_code`),
  ADD KEY `FKfpu4jey3ewte3xgk1j8nagm87` (`book_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `FKfpu4jey3ewte3xgk1j8nagm87` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
