

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `devcamp_exam`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `books`
--

CREATE TABLE `books` (
  `id` bigint(20) NOT NULL,
  `chapter_code` varchar(10) DEFAULT NULL,
  `chapter_name` varchar(255) DEFAULT NULL,
  `introduce` varchar(255) DEFAULT NULL,
  `page_` bigint(20) DEFAULT NULL,
  `translator` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `books`
--

INSERT INTO `books` (`id`, `chapter_code`, `chapter_name`, `introduce`, `page_`, `translator`) VALUES
(1, 'BK1234', 'Lập Trình Java', 'Khóa học cho người mới', 12, 'Trần Minh Khanh'),
(2, 'BK2345', 'Lập Trình python', 'lập trình nâng cao', 15, 'Hoàng Hoàng'),
(3, 'BK3456', 'Lập Trình fontEnd', 'cấu trúc fontEnd', 255, 'Lê NGọc');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_aci17uoit57eg0u5c3rr48g6p` (`chapter_code`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `books`
--
ALTER TABLE `books`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
