package com.devcamp.exam.exam_crud_full.repository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.exam.exam_crud_full.model.CBook;

public interface IBookRepository extends CrudRepository<CBook,Long>{
    CBook findByChapterCode(String code);
    CBook findByLessonsLessonCode(String Lesson);
}
