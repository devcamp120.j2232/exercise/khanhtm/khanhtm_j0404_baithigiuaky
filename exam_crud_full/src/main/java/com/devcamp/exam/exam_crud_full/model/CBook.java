package com.devcamp.exam.exam_crud_full.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "books")
public class CBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Size(min = 3 , max = 10 , message = "nhập đúng sô kí tự cho phép")
    @NotBlank(message = "Please fill out the information completely")
    @Column(name = "chapter_code" , unique = true)
    private String chapterCode;

    @NotBlank(message = "Please fill out the information completely")
    @Column(name = "chapter_name")
    private String chapterName;

    @Column(name = "introduce")
    private String introduce;

    @NotBlank(message = "Please fill out the information completely")
    @Column(name = "translator")
    private String translator;

    @Range(min = 1 , message = "phải là số dương")
    @Column(name = "page_")
    private Long page;

    @OneToMany(mappedBy = "books" , cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    private Set<CLesson> lessons;

    public CBook() {
    }

    public CBook(Long id, String chapterCode, String chapterName, String introduce, String translator, Long page, Set<CLesson> lessons) {
        this.id = id;
        this.chapterCode = chapterCode;
        this.chapterName = chapterName;
        this.introduce = introduce;
        this.translator = translator;
        this.page = page;
        this.lessons = lessons;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChapterCode() {
        return chapterCode;
    }

    public void setChapterCode(String chapterCode) {
        this.chapterCode = chapterCode;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Set<CLesson> getlessons() {
        return lessons;
    }

    public void setlessons(Set<CLesson> lessons) {
        this.lessons = lessons;
    }

    

}
