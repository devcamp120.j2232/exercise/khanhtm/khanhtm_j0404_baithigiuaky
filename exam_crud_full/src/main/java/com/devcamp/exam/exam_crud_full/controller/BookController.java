package com.devcamp.exam.exam_crud_full.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.exam.exam_crud_full.model.CBook;
import com.devcamp.exam.exam_crud_full.repository.IBookRepository;

    @RestController
    @RequestMapping("/")
    @CrossOrigin(value = "*" , maxAge = -1)
    public class BookController {
        @Autowired
        private IBookRepository iBookRepository;

        @GetMapping("/books")
        public List<CBook> getAllBook(){
            List<CBook> listBook = new ArrayList<>();
            iBookRepository.findAll().forEach(listBook::add);
            return listBook;
        }


        @GetMapping("/books/{id}")
        public ResponseEntity<CBook> getBookById(@PathVariable("id")Long id){
            try{
                Optional<CBook> fBook = iBookRepository.findById(id);
                if(fBook.isPresent()){
                    return new ResponseEntity<CBook>(fBook.get(), HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
                }
            }catch(Exception ex){
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }


        @PostMapping("/books/create")
        public ResponseEntity<Object> createBook(@Valid @RequestBody CBook body){
            try{
             
                CBook nBook = new CBook();
                nBook.setChapterCode(body.getChapterCode());
                nBook.setChapterName(body.getChapterName());
                nBook.setIntroduce(body.getIntroduce());
                nBook.setPage(body.getPage());
                nBook.setTranslator(body.getTranslator());
                nBook.setlessons(body.getlessons());

               
                return new ResponseEntity<Object>(iBookRepository.save(nBook), HttpStatus.CREATED);
            }catch (Exception e) {
                System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
            }
        }

        @PutMapping("/books/update/{id}")
        public ResponseEntity<Object> updateBook(@PathVariable("id")Long id ,@Valid @RequestBody CBook body){
            Optional<CBook> fBook = iBookRepository.findById(id);
            if(fBook.isPresent()){
                CBook nBook = fBook.get();
                nBook.setChapterCode(body.getChapterCode());
                nBook.setChapterName(body.getChapterName());
                nBook.setIntroduce(body.getIntroduce());
                nBook.setPage(body.getPage());
                nBook.setTranslator(body.getTranslator());
                nBook.setlessons(body.getlessons());
                try{
                    return new ResponseEntity<Object>(iBookRepository.save(nBook), HttpStatus.OK);
                }catch (Exception e) {
                    System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                    return ResponseEntity.unprocessableEntity().body("Failed to update specified Voucher: "+e.getCause().getCause().getMessage());
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        @DeleteMapping("/books/delete/{id}")
        public ResponseEntity<Object> deleteBookById(@PathVariable Long id) {
            try {
                iBookRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        @GetMapping("/booksBy")
        public CBook getBookByLessonCode(@RequestParam(value = "lessonCode")String lessonCode){
            return iBookRepository.findByLessonsLessonCode(lessonCode);
        }


}
