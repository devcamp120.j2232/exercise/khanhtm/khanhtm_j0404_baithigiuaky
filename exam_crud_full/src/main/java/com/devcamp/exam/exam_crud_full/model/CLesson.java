package com.devcamp.exam.exam_crud_full.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "lessons")
public class CLesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Không được để trống code")
    @Size(min = 3 , max = 10 , message = "nhập đúng sô kí tự cho phép")
    @Column(name = "lesson_code" , unique = true)
    private String lessonCode;
    
    @NotBlank(message = "Không được để trống Tên")
    @Column(name = "lesson_name")
    private String lessonName;

    @Column(name = "introduce")
    private String introduce;

    @Range(min = 1 , message = "số trang phải là số dương")
    @Column(name = "page_")
    private Long page;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id" , nullable = false)
    @JsonIgnore
    private CBook books;

    public CLesson() {
    }

    public CLesson(Long id, String lessonCode, String lessonName, String introduce, Long page, CBook books) {
        this.id = id;
        this.lessonCode = lessonCode;
        this.lessonName = lessonName;
        this.introduce = introduce;
        this.page = page;
        this.books = books;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLessonCode() {
        return lessonCode;
    }

    public void setLessonCode(String lessonCode) {
        this.lessonCode = lessonCode;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public CBook getBooks() {
        return books;
    }

    public void setBooks(CBook books) {
        this.books = books;
    }

    
}
