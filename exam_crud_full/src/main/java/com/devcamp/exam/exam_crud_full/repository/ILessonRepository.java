package com.devcamp.exam.exam_crud_full.repository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.exam.exam_crud_full.model.CLesson;

public interface ILessonRepository extends CrudRepository<CLesson,Long>{
    CLesson findByLessonCode(String code);
    
}
