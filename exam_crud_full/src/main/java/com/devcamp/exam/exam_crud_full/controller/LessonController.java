package com.devcamp.exam.exam_crud_full.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.exam.exam_crud_full.model.CBook;
import com.devcamp.exam.exam_crud_full.model.CLesson;
import com.devcamp.exam.exam_crud_full.repository.IBookRepository;
import com.devcamp.exam.exam_crud_full.repository.ILessonRepository;


@RestController
@RequestMapping("/")
@CrossOrigin(value = "*" , maxAge = -1)
public class LessonController {
    @Autowired
    private ILessonRepository iLessonRepository;

    @Autowired
    private IBookRepository iBookRepository;

        @GetMapping("/lessons")
            public List<CLesson> getAllLesson(){
                List<CLesson> listLesson = new ArrayList<>();
                iLessonRepository.findAll().forEach(listLesson::add);
                return listLesson;
            }


        @GetMapping("/lessons/{id}")
        public ResponseEntity<CLesson> getLessonById(@PathVariable("id")Long id){
            try{
                Optional<CLesson> fLesson = iLessonRepository.findById(id);
                if(fLesson.isPresent()){
                    return new ResponseEntity<CLesson>(fLesson.get(), HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
                }
            }catch(Exception ex){
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }


        @PostMapping("/lessons/create/{id}")
        public ResponseEntity<Object> createLesson(@Valid  @RequestBody CLesson body , @PathVariable("id")Long id){
            Optional<CBook> fBook = iBookRepository.findById(id);
            if(fBook.isPresent()){
                CLesson nLesson = new CLesson();
                nLesson.setLessonName(body.getLessonName());
                nLesson.setLessonCode(body.getLessonCode());
                nLesson.setPage(body.getPage());
                nLesson.setIntroduce(body.getIntroduce());
                nLesson.setBooks(fBook.get());
                try{
                return new ResponseEntity<Object>(iLessonRepository.save(nLesson), HttpStatus.CREATED);
            }catch (Exception e) {
                System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
                }
            }else{
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        @PutMapping("/lessons/update/{id}")
        public ResponseEntity<Object> updateLesson(@PathVariable("id")Long id ,@Valid @RequestBody CLesson body){
            Optional<CLesson> fLesson = iLessonRepository.findById(id);
            if(fLesson.isPresent()){
                CLesson nLesson = fLesson.get();
                nLesson.setLessonName(body.getLessonName());
                nLesson.setLessonCode(body.getLessonCode());
                nLesson.setPage(body.getPage());
                nLesson.setIntroduce(body.getIntroduce());
                try{
                    return new ResponseEntity<Object>(iLessonRepository.save(nLesson), HttpStatus.OK);
                }catch (Exception e) {
                    System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                    return ResponseEntity.unprocessableEntity().body("Failed to update specified Voucher: "+e.getCause().getCause().getMessage());
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        @DeleteMapping("/lessons/delete/{id}")
        public ResponseEntity<Object> deleteLessonById(@PathVariable Long id) {
            try {
                iLessonRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
}
