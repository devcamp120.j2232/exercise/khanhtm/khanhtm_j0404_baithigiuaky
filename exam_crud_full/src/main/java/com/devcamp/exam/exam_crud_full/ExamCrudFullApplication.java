package com.devcamp.exam.exam_crud_full;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamCrudFullApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamCrudFullApplication.class, args);
	}

}
